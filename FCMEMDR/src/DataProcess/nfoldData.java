package DataProcess;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import MulitifactorDimensionalityReduction.Sample;

/**
 * The nfoldData defines the n-fold data set in the experiment. 
 * This class includes the samples and their corresponding n-fold.
 * */

public class nfoldData 
{	
	/**
	 * The total number of samples in the experiment.
	 * */
	
	private int m_sampleSize = 0;
	
	/**
	 * The random seed of the experiment, 
	 * 		in which the seed is used to randomly sort for generating the n-fold data.
	 * */
	
	private int m_seed = 0;
	
	/**
	 * The samples is stored by their corresponding n-fold.
	 * The String in Map is the number of fold.
	 * The ArrayList< Sample > in Map is the samples.
	 * */
	
	private Map< Integer , ArrayList< Sample > > m_samplesInclude_in_nfold = null;
	
	private Map< Integer , Map< Integer, Integer> > m_outcomeNumber_in_nfold = null;
	
	private Map< Integer , Map< Integer, Integer> > m_sumOutcomeNumber_in_nfold = null;
	
	/**
	 * Construct nfoldData which initiate the m_samplesInclude_in_nfold.
	 * */
	
	public nfoldData()
	{
			m_samplesInclude_in_nfold = new HashMap< Integer , ArrayList< Sample > >();
	}
	
	
	/**
	 * Sets the total number of samples in the experiment.
	 * 
	 * @param	size	The total number of samples in the experiment.
	 * */
	
	public void setSampleSize( int size )
	{
			m_sampleSize =size;
	}
	
	/**
	 * Return the total number of samples in the experiment.
	 * 
	 * @return	The total number of samples in the experiment.
	 * */
	
	public int getSampleSize()
	{
			return m_sampleSize;
	}
	
	/**
	 * Sets the random seed in the experiment.
	 * 
	 * @param	seed The random seed in the experiment.
	 * */
	
	public void setSeed( int seed )
	{
			m_seed = seed;
	}
	
	/**
	 * Return the random seed in the experiment.
	 * 
	 * @return The random seed in the experiment.
	 * */
	
	public int getSeed()
	{
			return m_seed;
	}
	
	public void setOutcomeNumberInNFoldData( Integer n , Integer name , Integer size)
	{
		if(m_outcomeNumber_in_nfold != null)
		{
			if( m_outcomeNumber_in_nfold.containsKey( n ))
			{
				m_outcomeNumber_in_nfold.get( n ).put( name , size );
			}
			else
			{
				Map< Integer, Integer> sizeInNfold = new HashMap< Integer , Integer>();
				sizeInNfold.put( name , size );
				m_outcomeNumber_in_nfold.put( n , sizeInNfold );
			}
		}
		else
		{
			m_outcomeNumber_in_nfold = new HashMap< Integer , Map< Integer, Integer> >();
			setOutcomeNumberInNFoldData( n , name , size);
		}
	}
	
	public void sumOutcomeNumberInNFoldData()
	{
		m_sumOutcomeNumber_in_nfold = new HashMap< Integer , Map< Integer, Integer> >();
		Set<Integer> keys = m_outcomeNumber_in_nfold.keySet();
		for(int i=1 ; i <= keys.size() ; i++)
		{
			for(int j=1 ; j <= keys.size() ; j++)
			{
				if(i != j)
				{
					if(m_sumOutcomeNumber_in_nfold.containsKey(i))
					{
						Set<Integer> keyss = m_outcomeNumber_in_nfold.get(i).keySet();
						for(int k=0 ; k < keyss.size() ; k++)
						{
							m_sumOutcomeNumber_in_nfold.get(i).put((Integer)keyss.toArray()[k],
									m_sumOutcomeNumber_in_nfold.get(i).get(keyss.toArray()[k]) + 
									m_outcomeNumber_in_nfold.get(i).get(keyss.toArray()[k])
									);
						}
					}
					else
					{
						Map< Integer , Integer > sss = new HashMap< Integer , Integer >();
						sss.putAll(m_outcomeNumber_in_nfold.get(keys.toArray()[j-1]));
						m_sumOutcomeNumber_in_nfold.put(i, sss);
						
					}
				}
			}
		}
	}
	
	public Map< Integer , Map< Integer, Integer> > getSumOutcomeNumberInNFoldData()
	{
		return m_sumOutcomeNumber_in_nfold;
	}
	
	public Map< Integer , Map< Integer, Integer> > getOutcomeNumber_in_nfold()
	{
		return m_outcomeNumber_in_nfold;
	}
	
	/**
	 * Adds the samples into the specified n-fold.
	 * 
	 * @param	n	The specified n-fold.
	 * 
	 * @param	samples	The samples.
	 * */
	
	public void addnfoldData( Integer n , List< Sample > samples )
	{
		if( m_samplesInclude_in_nfold != null )
		{
			if( m_samplesInclude_in_nfold.containsKey( n ) )
			{
				m_samplesInclude_in_nfold.get( n ).addAll( samples );
			}
			else
			{
				ArrayList< Sample > sa = new ArrayList< Sample >();
				sa.addAll( samples );
				m_samplesInclude_in_nfold.put( n , sa );
			}
		}
		else 
		{
			m_samplesInclude_in_nfold = new HashMap< Integer , ArrayList< Sample > >();
			ArrayList< Sample > sa = new ArrayList< Sample >();
			sa.addAll( samples );
			m_samplesInclude_in_nfold.put( n , sa );
		}
	}
	
	/**
	 * Return the samples according specified n-fold.
	 * 
	 * @param	n	The specified n-fold.
	 * */
	
	public ArrayList< Sample > getnfoldData( int n )
	{
		if( m_samplesInclude_in_nfold != null )
		{
			return m_samplesInclude_in_nfold.get( n );
		}
		return null;
	}
	
	/**
	 * Return the samples which are stored by their corresponding n-fold.
	 * 
	 * @return The samples which are stored by their corresponding n-fold.
	 * */
	
	public Map< Integer , ArrayList< Sample > > getnfoldData()
	{
			return m_samplesInclude_in_nfold;
	}
	
	public ArrayList<Sample[]> getnfoldDataArrayList()
	{
		ArrayList<Sample[]> data = new ArrayList<Sample[]>();
		Iterator<Integer> nfold = m_samplesInclude_in_nfold.keySet().iterator();
		while(nfold.hasNext()) {
			ArrayList<Sample> samplesArrayList = m_samplesInclude_in_nfold.get(nfold.next());
			Sample[] samples = new Sample[samplesArrayList.size()];
			samples = samplesArrayList.toArray(samples);
			data.add(samples);
		}
		return data;
	}
	
	/**
	 * Return the number of n-fold.
	 * 
	 * @return	The number of n-fold.
	 * */
	
	public int getnfold()
	{
		return getnfoldData().keySet().size();
	}
	
}

/*
 * <p>
 * Copyright &copy; 2015-2016 National Kaohsiung University of Applied Sciences.
 * Author: Yu-Da Lin
 * </p>
 * <p>
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * </p>
 * <p>
 * This program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more
 * details.
 * </p>
 * <p>
 * You should have received a copy of the GNU General Public
 * License along with this program.
 * </p>
 */

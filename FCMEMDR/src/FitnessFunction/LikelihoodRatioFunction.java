package FitnessFunction;

public class LikelihoodRatioFunction implements fitnessFunction
{

	@Override
	public double getResults(double TP, double FP, double FN, double TN) {
		// TODO Auto-generated method stub
		double n1 = TP + FN;
		double n2 = FP + TN;
		double m1 = TP + FP;
		double m2 = FN + TN;
		double nm = n1 + n2;
		double eTP = (n1 * m1) / nm;
		double eFP = (n2 * m1) / nm;
		double eFN = (n1 * m2) / nm;
		double eTN = (n2 * m2) / nm;
		return 2 * ((TP * getLog(TP, eTP)) +
					(FP * getLog(FP, eFP)) +
					(FN * getLog(FN, eFN)) +
					(TN * getLog(TN, eTN))
					);
	}
	
	private double getLog(double value, double value2)
	{
		if(value == 0 || value2 ==0)
		{
			return 0;
		}else
		{
			return Math.log(value / value2);
		}
		
	}
	
	@Override
	public boolean getMinOrMax() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public String getFunctionName() {
		// TODO Auto-generated method stub
		return "LR";
	}

	@Override
	public int getFunctionCode() {
		// TODO Auto-generated method stub
		return 14;
	}
	
}

/*
 * <p>
 * Copyright &copy; 2018-2019
 * Author: Yu-Da Lin
 * Web: https://www.researchgate.net/profile/Yu-Da_Lin
 * </p>
 * <p>
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * </p>
 * <p>
 * This program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more
 * details.
 * </p>
 * <p>
 * You should have received a copy of the GNU General Public
 * License along with this program.
 * </p>
 */
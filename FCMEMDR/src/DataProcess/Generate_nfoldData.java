package DataProcess;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import MulitifactorDimensionalityReduction.Sample;

/**
 * A main panel in this program, and it aims to generate n-fold data set. 
 * */

public class Generate_nfoldData
{
	private int seed = 0;
	
	/**
	 * The informations of columns.
	 * */
	
	private ArrayList< Sample > allSamples = null;
	
	/**
	 * The samples in the n-fold data sets.
	 * */
	
	private Map< Boolean , ArrayList< Sample > > samplesInFold = null;
	
	/**
	 * The n-fold data sets.
	 * */
	
	private nfoldData nfolddata = null;			
	
	/**
	 * Constructs the Panel with Generate_nfoldData.
	 * */
	
	public Generate_nfoldData()
	{
		
	}
	
	public Generate_nfoldData(int seed)
	{
		this.seed = seed;
	}
	
	public void doGeneratenfoldData(ArrayList< Sample > column){
		this.allSamples = column;
		classifyTypeInSamples();
		randomSortSampleAccordingSeed();
		generatenfoldData();
	}
	
	/**
	 * Return the generated n-fold data sets.
	 * 
	 * @return	The generated n-fold data sets.
	 * */
	
	public nfoldData getnfoldData()
	{
		return nfolddata;
	}
	
	
	/**
	 * Generates the samples.
	 * */
	
	private void classifyTypeInSamples()
	{
			//Create a HashMap to collect the samples.
			samplesInFold = new HashMap< Boolean , ArrayList< Sample > >();
			
			//Add sample into Samples HashMap. 
			for( int s = 0 ; s < allSamples.size() ; s++ )
			{
				boolean type = allSamples.get(s).getType();
				if(samplesInFold.containsKey(type)){
					ArrayList< Sample > sa = samplesInFold.get( type );
					sa.add( allSamples.get(s) );
					samplesInFold.put( type , sa );
				}else{
					ArrayList< Sample > sa = new ArrayList< Sample >();
					sa.add( allSamples.get(s) );
					samplesInFold.put( type , sa );
				}
			}		
	}
	
	/**
	 * Randomly sort the samples in each type of outcome.
	 * */
	
	private void randomSortSampleAccordingSeed()
	{
			Random r = new Random();
			r.setSeed( seed );
			for( int i = 0 ; i < samplesInFold.keySet().size() ; i++ )
			{
				boolean name = (boolean) samplesInFold.keySet().toArray()[ i ];
				Collections.shuffle( samplesInFold.get( name ) , r );
			}
	}
	
	/**
	 * Generates the n-fold data sets after the randomSortSampleAccordingSeed().
	 * */
	
	private void generatenfoldData()
	{
		//Get the number of fold
		//Create the nfoldData.
		nfolddata = new nfoldData();
		nfolddata.setSeed(seed);	//Records the seed in the experiment.
		nfolddata.setSampleSize(allSamples.size()); //Records the sample size in the experiment.
		
		/**
		 * Add samples into nfoldData.
		 * */
		for( int i = 0 ; i < samplesInFold.keySet().size() ; i++ )
		{
			boolean type = (boolean) samplesInFold.keySet().toArray()[ i ];
			int region = samplesInFold.get( type ).size()/5;
			int n = 5;
			for( int j = 0 ; j < n - 1 ; j++ )
			{
				nfolddata.addnfoldData( j + 1 , samplesInFold.get( type ).subList( j * region , ( j + 1 ) * region ) );
			}
			nfolddata.addnfoldData( n , samplesInFold.get( type ).subList( ( n - 1 ) * region , samplesInFold.get( type ).size() ) );
		}
		
	}
}

/*
 * <p>
 * Copyright &copy; 2015-2017
 * Author: Yu-Da Lin
 * </p>
 * <p>
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * </p>
 * <p>
 * This program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more
 * details.
 * </p>
 * <p>
 * You should have received a copy of the GNU General Public
 * License along with this program.
 * </p>
 */

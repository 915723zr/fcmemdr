package DataProcess;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import MulitifactorDimensionalityReduction.Sample;

/**
 * The LoadData provides the operations of reading file and reading JTable object.
 * */

public class LoadData 
{
	private String[] m_heading = null;
	
	/**
	 * Construct LoadData.
	 * */
	
	public LoadData()
	{
		
	}
	
	public String[] getHeading()
	{
		return m_heading;
	}
	
	/**
	 * The readFile provides the operations of reading file.
	 * */
	
	public class readFile extends columnData
	{
		/**
		 * Construct readFile which can hold the file name.
		 * 
		 * @param	fileName	The file path.
		 * */
		
		public readFile( String fileName )
		{
			String strLine = "";
			try 
			{
				FileReader fr = new FileReader( fileName );
				BufferedReader bfr = new BufferedReader( fr );
				boolean flag = true;
				String dataType = "";
				while( ( strLine=bfr.readLine() ) != null )
				{
					if(flag)
					{
						if(strLine.contains("\t")) 
						{
							dataType = "\t";
						}else if (strLine.contains(","))
						{
							dataType = ",";
						}else
						{
							System.out.println("The dataset type is mistake.");
						}
						m_heading = strLine.split( dataType );
						flag = false;
					}else{
						String[] values = strLine.split( dataType ); 
						int[] numvalues = new int[values.length-1];
						for(int i=0;i<values.length-1;i++){
							numvalues[i] = Integer.valueOf(values[i]);
						}
						m_data_transfered.add( new Sample(numvalues,values[values.length-1].equals("0")?false:true) );
					}
					
				}
				bfr.close();
				fr.close();
			} 
			catch( IOException e ) 
			{
				System.err.println( e );
			} 
			finally 
			{
				
			}
		}
	}
		
	/**
	 * The columnData provides the information of column.
	 * */
	
	
	public class columnData
	{
		
		/**
		 * The values of columns in the data set.
		 * */
		
		ArrayList< Sample > m_data_transfered = new ArrayList< Sample >();
		
		/**
		 * The types of columns in the data set.
		 * */
		
		boolean[] m_columnType = null; //true: Numeric,false: Category
		
		public ArrayList< Sample > getData()
		{
			return m_data_transfered;
		}

	}
}

/*
 * <p>
 * Copyright &copy; 2015-2016 National Kaohsiung University of Applied Sciences.
 * Author: Yu-Da Lin
 * </p>
 * <p>
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * </p>
 * <p>
 * This program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more
 * details.
 * </p>
 * <p>
 * You should have received a copy of the GNU General Public
 * License along with this program.
 * </p>
 */

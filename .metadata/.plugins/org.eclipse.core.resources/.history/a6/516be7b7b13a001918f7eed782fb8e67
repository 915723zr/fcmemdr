package Pareto;

import java.util.ArrayList;
import java.util.Arrays;

import FitnessFunction.Fitnesses;
import MulitifactorDimensionalityReduction.Cell;
import MulitifactorDimensionalityReduction.TestData;
import MulitifactorDimensionalityReduction.TrainingData;

public class OptimalSolutions 
{
	private ArrayList<TrainingData> paretoOptimalTrainingData = null;
	private ArrayList<TestData> paretoTesting = null;
	private final int DOMINATE = 0;
	private final int DOMINATED = 2;
	private Compare compare = new Compare();
	
	public OptimalSolutions()
	{
		paretoOptimalTrainingData = new ArrayList<TrainingData>();
		paretoTesting = new ArrayList<TestData>();
	}
		
	public void addTrainingData( TrainingData solution )
	{
		if( paretoOptimalTrainingData.size() != 0 )
		{
			boolean flag = true;
			ArrayList<Integer> index = new ArrayList<Integer>();			
			for( int i = 0 ; i < paretoOptimalTrainingData.size() ; i++ )
			{
				int value = isDominated( solution , paretoOptimalTrainingData.get( i ) );
				
				if(value == DOMINATED)
				{
					flag = false;
					break;
				}
				else if(value == DOMINATE)
				{
					index.add( i );
				}
			}
			if ( flag )
			{
				for( int i = index.size()-1 ; i >= 0 ; i-- )
				{
					paretoOptimalTrainingData.remove( index.get( i ).intValue() );
				}
				paretoOptimalTrainingData.add( solution );
			}
		}
		else if( paretoOptimalTrainingData.size() == 0 )
		{
			paretoOptimalTrainingData.add( solution );
		}
	}
	
	private int isDominated( TrainingData v1 , TrainingData v2 )
	{
		return compare.isDominated( v1 , v2 );
	}
		
	public TrainingData getParetoOptimaTrainingData(int index)
	{
		return paretoOptimalTrainingData.get(index);
	}
	
	public ArrayList<TrainingData> getParetoOptimaTrainingData()
	{
		return paretoOptimalTrainingData;
	}	
	
	public void setParetoTestingData(TestData testData)
	{
		paretoTesting.add(testData);
	}
	
	public ArrayList<TestData> getParetoOptimaTestData()
	{
		return paretoTesting;
	}	
	
	public void showPareto()
	{
		for( int i = 0 ; i < paretoOptimalTrainingData.size() ; i++ )
		{
			int index = 1;
			System.out.println("Pareto " + index);
			System.out.println(Arrays.toString(paretoOptimalTrainingData.get(i).getIndexSNP()));
			for(Cell cell:paretoOptimalTrainingData.get(i).getFactorCollection().values()){
				System.out.println(index++ + ":\tcase:\t"+cell.getTotalCase() +
											"\tcontrol:\t"+cell.getTotalControl() +
											"\t" + cell.getCellValue());
			}
			System.out.println("TPfuzzy = " + paretoOptimalTrainingData.get(i).getTP() + "\n" +
								"FPfuzzy = " + paretoOptimalTrainingData.get(i).getFP() + "\n" +
								"FNfuzzy = " + paretoOptimalTrainingData.get(i).getFN() + "\n" +
								"TNfuzzy = " + paretoOptimalTrainingData.get(i).getTN() + "\n");
			for(Fitnesses fitness:paretoOptimalTrainingData.get(i).getTrainingEstimation().getFitnesses().values())
			{				
				System.out.print(fitness.getFunctionName() + ":\t" + fitness.getValue() + "\t");
			}
			System.out.println("test:");
			System.out.println("TPfuzzy = " + paretoTesting.get(i).getTP() + "\n" +
								"FPfuzzy = " + paretoTesting.get(i).getFP() + "\n" +
								"FNfuzzy = " + paretoTesting.get(i).getFN() + "\n" +
								"TNfuzzy = " + paretoTesting.get(i).getTN() + "\n");
			for(Fitnesses fitness:paretoTesting.get(i).getMultiObjectiveSolution().getFitnesses().values())
			{				
				System.out.print(fitness.getFunctionName() + ":\t" + fitness.getValue() + "\t");
			}
		}
	}
}

/*
 * <p>
 * Copyright &copy; 2018-2019
 * Author: Yu-Da Lin
 * Web: https://www.researchgate.net/profile/Yu-Da_Lin
 * </p>
 * <p>
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * </p>
 * <p>
 * This program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more
 * details.
 * </p>
 * <p>
 * You should have received a copy of the GNU General Public
 * License along with this program.
 * </p>
 */

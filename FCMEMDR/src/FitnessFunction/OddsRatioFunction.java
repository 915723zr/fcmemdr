package FitnessFunction;

import Math.CIvalue;

public class OddsRatioFunction implements fitnessFunction
{
	
	@Override
	public double getResults(double TP, double FP, double FN, double TN) 
	{
		// TODO Auto-generated method stub
		return (TP / FP) / (FN / TN);
	}
	
	public CIvalue getConfidenceInterval(double TP, double FP, double FN, double TN)
	{
		double or = getResults(TP, FP, FN, TN);
		double natural = Math.log(or);
		double se = Math.sqrt(1 / TP + 1 / FP + 1 / FN + 1 / TN);
		double upper = Math.exp((natural + 1.96 * se));
		double lower = Math.exp((natural - 1.96 * se));
		return new CIvalue(or, upper, lower);
	}

	@Override
	public boolean getMinOrMax() 
	{
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public String getFunctionName() {
		// TODO Auto-generated method stub
		return "OR";
	}

	@Override
	public int getFunctionCode() {
		// TODO Auto-generated method stub
		return 6;
	}
	
}

/*
 * <p>
 * Copyright &copy; 2018-2019
 * Author: Yu-Da Lin
 * Web: https://www.researchgate.net/profile/Yu-Da_Lin
 * </p>
 * <p>
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * </p>
 * <p>
 * This program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more
 * details.
 * </p>
 * <p>
 * You should have received a copy of the GNU General Public
 * License along with this program.
 * </p>
 */

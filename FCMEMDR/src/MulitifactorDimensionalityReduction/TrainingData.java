package MulitifactorDimensionalityReduction;

import java.util.Map;

import FitnessFunction.fitnessFunction;
import Solution.ObjectiveSolution;

public class TrainingData extends FactorCollection
{
	
	private ObjectiveSolution m_estimation = null;
	private int[] m_indexSNP = null;
	private double m_TP = 0.0d;
	private double m_FP = 0.0d;
	private double m_FN = 0.0d;
	private double m_TN = 0.0d;
	private double m_High_Number = 0.0d;
	private double m_Low_Number = 0.0d;
	
	
	public TrainingData(Data data, int[] indexSNP, int seed)
	{
		
		super(data,indexSNP, seed, true);
		m_indexSNP = indexSNP.clone();
	}	
	
	public Map<Integer, Cell> getTrainingData()
	{
		return super.getFactorCollection();
	}
	
	public int[] getIndexSNP()
	{
		return m_indexSNP;
	}
	
	public ObjectiveSolution getTrainingEstimation()
	{
			m_estimation = new ObjectiveSolution(getIndexSNP());
			m_TP = 0;
			m_FP = 0;
			m_FN = 0;
			m_TN = 0;
			m_High_Number = 0.0d;
			m_Low_Number = 0.0d;
			for(Cell cell:super.getFactorCollection().values()){
				if(cell.getGroupType()){
					m_TP += cell.getTotalCase();
					m_FP += cell.getTotalControl();
					m_High_Number++;
				}else {
					m_TN += cell.getTotalControl();
					m_FN += cell.getTotalCase();
					m_Low_Number++;
				}
			}
			
			for(fitnessFunction functions:MultifactorDimensionalityReduction.functions)
			{
				m_estimation.setFitnessFunction(functions, functions.getResults(m_TP, m_FP, m_FN, m_TN));
			}
			
			
			return m_estimation;
	}
	
	public ObjectiveSolution getEstimation()
	{
		if(m_estimation==null){
			getTrainingEstimation();
		}
		return m_estimation;
	}
	
	public void setTP(double TP){this.m_TP = TP;}
	public void setFP(double FP){this.m_FP = FP;}
	public void setFN(double FN){this.m_FN = FN;}
	public void setTN(double TN){this.m_TN = TN;}
	public double getTP(){return m_TP;}
	public double getFP(){return m_FP;}
	public double getFN(){return m_FN;}
	public double getTN(){return m_TN;}
	public double getHighNumber(){return m_High_Number;}
	public double getLowNumber(){return m_Low_Number;}
	
}

/*
 * <p>
 * Copyright &copy; 2015-2019
 * Author: Yu-Da Lin
 * Web: https://www.researchgate.net/profile/Yu-Da_Lin
 * </p>
 * <p>
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * </p>
 * <p>
 * This program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more
 * details.
 * </p>
 * <p>
 * You should have received a copy of the GNU General Public
 * License along with this program.
 * </p>
 */

package MulitifactorDimensionalityReduction;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import FitnessFunction.Fitnesses;
import FitnessFunction.OddsRatioFunction;
import FitnessFunction.PvalueFunction;
import Math.CIvalue;
import Pareto.OptimalSolutions;

public class DetermineBestSolutionsInParetoSet 
{
	BackResult backResult = new BackResult();
	private ArrayList<OptimalSolutions> m_ParetoOptimalSolutionsInCV = null;
	
	public DetermineBestSolutionsInParetoSet()
	{
		m_ParetoOptimalSolutionsInCV = new ArrayList<OptimalSolutions>();
	}	
	public void add(OptimalSolutions paretoTrainingData)
	{
		m_ParetoOptimalSolutionsInCV.add(paretoTrainingData);
	}	
	
	public int getTrainingDateNumber()
	{
		return m_ParetoOptimalSolutionsInCV.size();
	}	
	
	public void determineBestSolutionsInParetoSet()
	{
		Map<Integer, solution> countSolution = new HashMap<Integer, solution>();
		for(int i = 0 ; i < m_ParetoOptimalSolutionsInCV.size() ; i++)
		{
			int paretoSize = m_ParetoOptimalSolutionsInCV.get(i).getParetoOptimaTrainingData().size();
			for(int j = 0 ; j < paretoSize ; j++)
			{
				TrainingData trainingData = m_ParetoOptimalSolutionsInCV.get(i).getParetoOptimaTrainingData().get(j);
				int keys = 0;
				int ss = 1;
				for(int k=0 ; k < trainingData.getIndexSNP().length ; k++)
				{
					keys += trainingData.getIndexSNP()[k]*ss;
					ss *= 10;
				}
				
				if(countSolution.containsKey(keys))
				{
					countSolution.get(keys).increaseNumber(i+1);
				}
				else
				{
					countSolution.put(keys, new solution(trainingData.getIndexSNP(), i+1));
				}
			}
		}
		
		for(solution s:countSolution.values())
		{
			backResult.appendWhole(Arrays.toString(s.getSNPs()) + "\t" + s.getNumber() + "\t" + Arrays.toString(s.getIndex())+"\n");
		}
		
	}
	
	public void setFunctionName(String functionName)
	{
		backResult.setFunctionName(functionName);
	}
	
	public void PrintParetoSet()
	{
		for(int i = 0 ; i < m_ParetoOptimalSolutionsInCV.size() ; i++)
		{
			backResult.appendFold("************************** The " + (i+1) + "-fold **************************\n");
			int paretoSize = m_ParetoOptimalSolutionsInCV.get(i).getParetoOptimaTrainingData().size();
			backResult.appendFold("Find " + paretoSize + " models\n");
			for(int j = 0 ; j < paretoSize ; j++)
			{
				backResult.appendFold("In " + (j+1) + " model:\n");
				TrainingData trainingData = m_ParetoOptimalSolutionsInCV.get(i).getParetoOptimaTrainingData().get(j);
				backResult.appendFold("SNPs: " + Arrays.toString(trainingData.getIndexSNP()) + "\n");
				backResult.appendFold("Training data:\n");
				for(Fitnesses fitness:trainingData.getTrainingEstimation().getFitnesses().values())
				{
					backResult.appendFold(fitness.getFunctionName() + ": " + fitness.getValue() + "\t");
				}
				backResult.appendFold("\n");
				double TP = trainingData.getTP();
				double FP = trainingData.getFP();
				double FN = trainingData.getFN();
				double TN = trainingData.getTN();
				backResult.appendFold("TP: " + TP + "\tFP: " + FP + "\tFN: " + FN + "\tTN: " + TN + "\n");
				CIvalue or = new OddsRatioFunction().getConfidenceInterval(TP, FP, FN, TN);
				double pvalue = new PvalueFunction().getResults(TP, FP, FN, TN);
				backResult.appendFold("Odds ratio: " + or.getOddsRatio() + "\t( " + or.getLower() + ", " + or.getUpper() + ")\t" + pvalue + "\n");
				
				//-------------test data----------------
				backResult.appendFold("Test data:\n");
				TestData testData = m_ParetoOptimalSolutionsInCV.get(i).getParetoOptimaTestData().get(j);
				
				for(Fitnesses fitness:testData.getMultiObjectiveSolution().getFitnesses().values())
				{
					backResult.appendFold(fitness.getFunctionName() + ": " + fitness.getValue() + "\t");
				}
				backResult.appendFold("\n");
				TP = testData.getTP();
				FP = testData.getFP();
				FN = testData.getFN();
				TN = testData.getTN();
				backResult.appendFold("TP: " + TP + "\tFP: " + FP + "\tFN: " + FN + "\tTN: " + TN + "\n");
				or = new OddsRatioFunction().getConfidenceInterval(TP, FP, FN, TN);
				pvalue = new PvalueFunction().getResults(TP, FP, FN, TN);
				backResult.appendFold("Odds ratio: " + or.getOddsRatio() + "\t( " + or.getLower() + ", " + or.getUpper() + ")\t" + pvalue + "\n");
				
			}
			backResult.appendFold("\n\n\n");
		}
	}	
	
	public BackResult getResultString()
	{
		return backResult;
	}
}

class solution
{
	private int number = 1;
	int[] SNPs = null;
	ArrayList<Integer> indexOfCV = new ArrayList<Integer>();
	public solution(int[] SNPs, int index)
	{
		this.SNPs = SNPs.clone();
		indexOfCV.add(index);
	}
	public void increaseNumber(int index)
	{
		number++;
		indexOfCV.add(index);
	}
	public int getNumber()
	{
		return number;
	}
	public int[] getSNPs()
	{
		return SNPs;
	}
	public Integer[] getIndex()
	{
		return indexOfCV.toArray(new Integer[indexOfCV.size()]);
	}
}

/*
 * <p>
 * Copyright &copy; 2018-2019
 * Author: Yu-Da Lin
 * Web: https://www.researchgate.net/profile/Yu-Da_Lin
 * </p>
 * <p>
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * </p>
 * <p>
 * This program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more
 * details.
 * </p>
 * <p>
 * You should have received a copy of the GNU General Public
 * License along with this program.
 * </p>
 */

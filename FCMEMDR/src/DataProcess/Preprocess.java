package DataProcess;

import java.io.File;
import java.util.ArrayList;

import MulitifactorDimensionalityReduction.Sample;

public class Preprocess
{
	private File file = null;
	private LoadData loadData= null;
	private ArrayList<Sample> dataset= null;
	private Generate_nfoldData generator = null;
	private int seed = 0;
	
	public Preprocess(File file , int seed)
	{
		this.file = file;
		this.seed = seed;
		dataPreprocess();
	}
	
	private void dataPreprocess()
	{
		openDataSet();
		generateNfoldData();
	}
	
	private void openDataSet()
	{
		loadData();
	}
	
	private void loadData()
	{
		loadData = new LoadData();
		dataset = loadData.new readFile(file.getPath()).getData();
		System.out.println(dataset.size());
	}
	
	
	private void generateNfoldData()
	{
		generator = new Generate_nfoldData(seed);
		generator.doGeneratenfoldData(dataset);
	}
	
	public ArrayList<Sample> getPreprocessDataSet()
	{
		return dataset;
	}
	
	public nfoldData getNfoldData()
	{
		return generator.getnfoldData();
	}
		
}

/*
 * <p>
 * Copyright &copy; 2015-2019
 * Author: Yu-Da Lin
 * Web: https://www.researchgate.net/profile/Yu-Da_Lin
 * </p>
 * <p>
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * </p>
 * <p>
 * This program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more
 * details.
 * </p>
 * <p>
 * You should have received a copy of the GNU General Public
 * License along with this program.
 * </p>
 */

* FCMEMDR is introduced in the article “Epistasis Analysis Using an Improved Fuzzy C-means-based Entropy Approach,” which is published in IEEE Transactions on Fuzzy Systems. 
* An improved fuzzy c-means-based entropy (FCME) approach was proposed to address the limitations of binary classification. 
* In this approach, the degree of membership in MDR is used. The FCME approach and MDR measure were integrated to enable more precise differentiation between similar frequencies of multifactor genotypes in cases of possible epistasis.
* 
*
* This code implements FCMEMDR in Java. 
* The “src/MAIN” folder contains the “main” of FCMEMDR for Java. 
* The “Samples” folder contains a data set. 
* The results can be obtained in the “Results” folder.


Copyright &copy; 2018-..
 * Authors: Cheng-Hong Yang, Li-Yeh Chuang, and Yu-Da Lin
 * </p>
 * <p>
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * </p>
 * <p>
 * This program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more
 * details.
 * </p>
 * <p>
 * You should have received a copy of the GNU General Public
 * License along with this program.
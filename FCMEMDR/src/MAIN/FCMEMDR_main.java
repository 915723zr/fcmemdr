package MAIN;

import java.io.File;
import java.io.FileNotFoundException;

import MulitifactorDimensionalityReduction.BackResult;
import MulitifactorDimensionalityReduction.DetermineBestSolution;
import MulitifactorDimensionalityReduction.MultifactorDimensionalityReduction;

public class FCMEMDR_main 
{
	
	/**
	 * The parameters of FCMEMDR.
	 * */
	private String functionName = "aCCR"; //aCCR or LR
	
	/**
	 * The default of data pathway is ..//FCMEMDR//Samples//
	 * The dataPathway allows the data name when the data set is located at default of data pathway.
	 * It also allows the absolute pathway when the data set is located at particular pathway.
	 * */
	private String dataPathway = "Dataset.txt"; 
	
	/**
	 * The number of locus combinations.
	 * */
	private int numberOfOrder = 2;
	
	/**
	 * The number of CV.
	 * */
	private int numberOfCV = 5;
	
	
	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		try {
			new FCMEMDR_main();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			System.out.println("Please check the file pathway.");
		}
	}
	
	public FCMEMDR_main() throws FileNotFoundException 
	{
		run();
	}
	
	private void run() throws FileNotFoundException 
	{		
		if (checkDataPathway(dataPathway)) 
		{
			File file = new File(dataPathway);
			MultifactorDimensionalityReduction MDR = new MultifactorDimensionalityReduction();
			BackResult result = MDR.implement(file, functionName.split(","), 0, functionName, numberOfOrder, numberOfCV);
			System.out.println("Done");
			result.toSaveData(file.getName());
			new DetermineBestSolution().determineBestSolution(result.getBestSolutions());
		}else {
			throw new FileNotFoundException();
		}
		
	}
	
	private boolean checkDataPathway(String pathway) 
	{
		File file = new File(pathway);
		if (file.isFile() != true) 
		{
			if(pathway.length()==0) 
			{
				return false;
			}else if(pathway.contains("Samples")!=true){
				String new_pathway = System.getProperty( "user.dir" ) + "\\Samples\\" + pathway;
				return checkDataPathway(new_pathway);
			}
			return false;
		}
		dataPathway = pathway;
		return true;
	}

}

/*
 * <p>
 * Copyright &copy; 2018-2019
 * Author: Yu-Da Lin
 * Web: https://www.researchgate.net/profile/Yu-Da_Lin
 * </p>
 * <p>
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * </p>
 * <p>
 * This program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more
 * details.
 * </p>
 * <p>
 * You should have received a copy of the GNU General Public
 * License along with this program.
 * </p>
 */

